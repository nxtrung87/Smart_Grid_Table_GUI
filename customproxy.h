// Copyright (C) 2016 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR BSD-3-Clause

#ifndef CUSTOMPROXY_H
#define CUSTOMPROXY_H

#include <QTimeLine>
#include <QGraphicsProxyWidget>
#include "Global_Modules.h"



class CustomProxy : public QGraphicsProxyWidget
{Q_OBJECT
public:
    enum table { table_1, table_2, table_3, table_4, table_5,table_6};
    table tableType(){return myTable;}
    explicit CustomProxy(QGraphicsItem *parent = nullptr, Qt::WindowFlags wFlags = { });

    QRectF boundingRect() const override;
    void paintWindowFrame(QPainter *painter, const QStyleOptionGraphicsItem *option,
                          QWidget *widget) override;
    QGraphicsScene *Scene;
protected:
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event) override;
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) override;
    //bool sceneEventFilter(QGraphicsItem *watched, QEvent *event) override;

    //void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent) override;

    QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;

private slots:
    void updateStep(qreal step);
    void stateChanged(QTimeLine::State);
    void zoomIn();
    void zoomOut();

private:
    QTimeLine *timeLine;
    QGraphicsItem *currentPopup = nullptr;
    bool popupShown = false;
    table myTable;
    table1 *Table1;
    table2 *Table2;
    table3 *Table3;
    table4 *Table4;
    table5 *Table5;
    table6 *Table6;

};

#endif // CUSTOMPROXY_H
