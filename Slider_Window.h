/******************************************************************************
 * Project    : Smart Grid Table GUI
 * Author     : Dingzhu Chen
 *
 * Description: header file for size adjustment class
******************************************************************************/
#ifndef SLIDER_WINDOW_H
#define SLIDER_WINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QLineEdit>
#include <QLayout>
#include <QLabel>
#include <QComboBox>
#include <QMessageBox>
#include <QDialog>
#include <QListView>

//#include "myslider.h"
#include "slidersgroup.h"
#include <QDebug>
#include <QObject>
#include <QSlider>
#include <QtWidgets>
#include <iostream>
using namespace std;
class Slider_Window : public QMainWindow
{
    Q_OBJECT

public:
    qreal num;
    //MySlider *slider;
    Slider_Window *GUI_slider;
       SlidersGroup *verticalSliders;
       QStackedWidget *stackedWidget;
       QSpinBox *valueSpinBox;
       QPushButton *max;
       QPushButton *medium;
       QPushButton *minimum;
    QMainWindow *my_slider;
    Slider_Window(QWidget *parent = NULL):QMainWindow(parent)
    {
        verticalSliders = new SlidersGroup(Qt::Vertical, tr("Vertical"));
verticalSliders->setMinimum(40);
verticalSliders->setMaximum(86);

        stackedWidget = new QStackedWidget;

        stackedWidget->addWidget(verticalSliders);
        //

        valueSpinBox = new QSpinBox;
        valueSpinBox->setRange(30, 100);
        valueSpinBox->setSingleStep(1);
        valueSpinBox->setParent(this);
        valueSpinBox->show();
        valueSpinBox->resize(60,20);
        valueSpinBox->move(0,0);
        connect(verticalSliders, &SlidersGroup::valueChanged,
                valueSpinBox, &QSpinBox::setValue);
        connect(verticalSliders, &SlidersGroup::valueChanged,
                this, &Slider_Window::ShowValue);


        stackedWidget->setParent(this);
        stackedWidget->move(0,100);
        stackedWidget->resize(300,300);
        stackedWidget->show();

        max =new QPushButton;
        max->resize(100, 100);
        max->move(200, 0);
        max->setText("Maximum");
        max->setStyleSheet("background-color: rgb(255, 0, 0);");
        max->setParent(this);
        max->show();
        connect(max,SIGNAL(clicked()),this, SLOT(MAX()));


        medium =new QPushButton;
        medium->resize(80, 80);
        medium->move(100, 10);
        medium->setText("Medium");
        medium->setStyleSheet("background-color: rgb(255, 0, 0);");
        medium->setParent(this);
        medium->show();
        connect(medium,SIGNAL(clicked()),this, SLOT(MEDIUM()));

        minimum =new QPushButton;
        minimum->resize(70, 70);
        minimum->move(0, 20);
        minimum->setText("Minimum");
        minimum->setStyleSheet("background-color: rgb(255, 0, 0);");
        minimum->setParent(this);
        minimum->show();
        connect(minimum,SIGNAL(clicked()),this, SLOT(SMALL()));

    }
signals:
    /*signals don't have return value,they can have parameters,
    signal functions don't need implementations, they only need to be declare*/
    void slider_update(int a);


public slots:
    void ShowValue(){
        //if(0.4<=num<=1){
        //printf("value=%d",valueSpinBox->value());
        num =(qreal)valueSpinBox->value()/100;
        cout<<"value:"<<num<<endl;

        emit slider_update(888);
        //}
    };
    void MAX(){
        num=1;
        emit slider_update(888);
    }

    void MEDIUM(){
        num=0.63;
        emit slider_update(888);
    }
    void SMALL(){
        num=0.53;
        emit slider_update(888);
    }
};





#endif // SLIDER_WINDOW_H
