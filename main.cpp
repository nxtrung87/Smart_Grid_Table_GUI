/******************************************************************************
 * Project    : Smart Grid Table GUI
 * Author     : Dingzhu Chen
 *
 * Description: Implementation file for main processing
******************************************************************************/
// Copyright (C) 2016 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR BSD-3-Clause

#include "customproxy.h"
#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <customproxy.h>
#include "mainwindow.h"

int main(int argc, char *argv[])
{

    QApplication a(argc, argv);
    mainwindow w;
    w.show();
    return a.exec();
}




















