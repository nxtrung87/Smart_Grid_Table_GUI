/******************************************************************************
 * Project    : Smart Grid Table GUI
 * Author     : Dingzhu Chen
 *
 * Description: function implementations of the constructor
******************************************************************************/
#include "global_items.h"

namespace global
{

}

/*this constructor is mainly used for reading value fdrom local json file
and store the "key to value structure data into map containers"*/
Global_Items::Global_Items()
{
    QString readonly_config = readonly_string("D:/graduation/graduation/Graduation_internship_code/embedded tables/3/embedded tables/embeddeddialogs/Scenario1_default.json");
    // 开始解析 解析成功返回QJsonDocument对象否则返回null
    QJsonParseError err_rpt;
    QJsonDocument root_document = QJsonDocument::fromJson(readonly_config.toUtf8(), &err_rpt);
    //QJsonParseError err_rpt;
    QJsonDocument  root_Doc = QJsonDocument::fromJson(readonly_config.toUtf8(), &err_rpt);//字符串格式化为JSON
    QJsonObject root_Obj = root_Doc.object();
    QJsonValue RFID_ID = root_Obj.value("1072051546");//每个object都是以大括号为识别标准和范围
    QJsonObject RFID_OBJ = RFID_ID.toObject();//toObject()形成一个obj

    QJsonValue JSON_ITEMS;
    const QStringList ITEM_LISTS = root_Obj.keys();
    for(const QString &item_key:ITEM_LISTS)
    {
      JSON_ITEMS  = root_Obj.value(item_key);
      MQTT_JSON_MAP_LOAD.insert(item_key,JSON_ITEMS.toObject().value("loads").toArray().at(0).toObject().value("p_set").toDouble());
      MQTT_JSON_MAP_GEN.insert(item_key,JSON_ITEMS.toObject().value("generators").toArray().at(0).toObject().value("p_nom_max").toDouble());
    }
}
